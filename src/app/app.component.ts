import {Component, OnInit} from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import {Feature, getUid} from "ol";
import {Point, Polygon} from "ol/geom";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  map: Map | undefined;
  markerId: string = '';
  gridId: string = '';
  isMarkerPresent: boolean = false;
  isGridPresent: boolean = false;
  mainFormGroup: FormGroup = new FormGroup({
    markerLat: new FormControl(null),
    markerLong: new FormControl(null),
    gridSize: new FormControl(null),
    cellSize: new FormControl(null),
    pollutionStrength: new FormControl(null)
  });

  ngOnInit(): void {
    this.map = new Map({
      target: 'map',
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
      ],
      view: new View({
        projection: 'EPSG:4326', //wg84
        center: [16, 45],
        zoom: 7,
      })
    });
  }

  setMarker(event: any): void {
    if (this.isMarkerPresent) return;
    let coords: number[] | undefined = [0, 0];
    if (this.map == undefined) {
      return
    }
    coords = this.map.getEventCoordinate(event);
    const layer = new VectorLayer({
      source: new VectorSource({
        features: [new Feature({
          geometry: new Point(coords)
        })]
      })
    });
    this.markerId = getUid(layer);
    this.isMarkerPresent = true;
    this.map.addLayer(layer);
    this.mainFormGroup.patchValue({
      markerLat: coords[0],
      markerLong: coords[1]
    });
  }

  generatePolygon(coords: number[][]): Polygon {
    return new Polygon([coords]);
  }

  setGrid(): void {
    const center = [this.mainFormGroup.value.markerLat, this.mainFormGroup.value.markerLong];
    const cellSize = this.mainFormGroup.value.cellSize;
    const gridSize = this.mainFormGroup.value.gridSize;
    const polygons: Feature<any>[] = [];
    const polygonSideSize = cellSize * 0.0000898311174991;
    let coords: number[][] = [];
    for (let i = 0; i < gridSize * 2 + 1; i++) {
      for (let j = 0; j < gridSize * 2 + 1; j++) {
        coords = [
          [(center[0] + (polygonSideSize / 2) + gridSize * polygonSideSize - i * polygonSideSize),
            (center[1] + (polygonSideSize / 2) + gridSize * polygonSideSize - j * polygonSideSize)],
          [(center[0] + (polygonSideSize / 2) + gridSize * polygonSideSize - i * polygonSideSize),
            (center[1] + (polygonSideSize / 2) + gridSize * polygonSideSize - j * polygonSideSize - polygonSideSize)],
          [(center[0] + (polygonSideSize / 2) + gridSize * polygonSideSize - i * polygonSideSize - polygonSideSize),
            (center[1] + (polygonSideSize / 2) + gridSize * polygonSideSize - j * polygonSideSize - polygonSideSize)],
          [(center[0] + (polygonSideSize / 2) + gridSize * polygonSideSize - i * polygonSideSize - polygonSideSize),
            (center[1] + (polygonSideSize / 2) + gridSize * polygonSideSize - j * polygonSideSize)]
        ];
        polygons.push(new Feature({
          geometry: this.generatePolygon(coords)
        }))
      }
    }
    const layer = new VectorLayer({
      source: new VectorSource({
        features: polygons
      })
    });
    this.gridId = getUid(layer);
    this.isGridPresent = true;
    this.map?.addLayer(layer);
  }

  clearMap(): void {
    this.map?.getLayers().getArray()
      .filter(layer => getUid(layer) == this.markerId || getUid(layer) == this.gridId)
      .forEach(layer => this.map?.removeLayer(layer));
    this.isMarkerPresent = false;
    this.isGridPresent = false;
  }

  addMarker() {
    const coords = [this.mainFormGroup.value.markerLat, this.mainFormGroup.value.markerLong];
    const layer = new VectorLayer({
      source: new VectorSource({
        features: [new Feature({
          geometry: new Point(coords)
        })]
      })
    });
    this.markerId = getUid(layer);
    this.isMarkerPresent = true;
    this.map?.addLayer(layer);
    this.mainFormGroup.patchValue({
      markerLat: coords[0],
      markerLong: coords[1]
    });
  }
}
